/*jshint node:true*/
'use strict';

var express = require('express');
var path = require('path');
var router = express.Router();

router.use('/', express.static(path.join(__dirname, 'dist')));

module.exports = router;
