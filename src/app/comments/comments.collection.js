(function () {
  'use strict';
  
  /**
   * Comments is a service for manage Comment model items.
   */
  
  angular.module('app.comments')
    .factory('Comments', factory);
  
  function factory (Comment) {
    
    // Collection constructor.
    function Comments () {}
    
    // Comments prototype is an simple JavaScript Array (for
    // ng-repeat support and another reasons).
    // Comments have API for manage collection and items.
    angular.extend(Comments.prototype = [], {
      
      // Create new Comment item with data.
      new: function (data) {
        var newCommment = new Comment(data);
        this.push(newCommment);
        return newCommment;
      },
      
      // Add several comments to collection.
      add: function (comments) {
        var me = this;
        
        angular.forEach(comments, function (comment) {
          me.new(comment);
        });
        
        return this;
      },
      
      // Remove comment from collection.
      remove: function (comment) {
        var index;
        while ((index = this.indexOf(comment)) >= 0) {
          this.splice(index, 1);
        }
        return this;
      },
      
      // Check for comment exists in collection.
      exists: function (comment) {
        return this.indexOf(comment) >= 0;
      },
      
    });
    
    return new Comments ();
  }
  
} ());
