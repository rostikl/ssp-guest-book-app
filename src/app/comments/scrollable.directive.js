(function () {
  'use strict';
  
  /**
   * scrollable directive using for any events about window scrolling:
   * - 'scrolledToFooter' when element's footer is over visible document;
   *
   * Usage:
   *  <div class="big" scrollable></div> // produces 'scrolledToFooter' event;
   *  <div id="Big" scrollable></div> // produces 'Big:scrolledToFooter' event;
   *  <div scrollable="BigFoot"></div> // produces 'BigFoot:scrolledToFooter' event;
   */
  
  angular.module('app.comments')
    .directive('scrollable', directive);
  
  function directive ($document, $window) {
    return {
      restrict: 'A',
      
      link: function (scope, element, attrs) {
        var $win = angular.element($window);
        var prefix = attrs.scrollable && (attrs.scrollable + ':') || element.attr('id') || '';
        
        var computeOffset = function () {
          var bottomXOnDocument = element.height() + element.offset().top;
          var globalBottomOffset = $win.height() - (bottomXOnDocument - $document.scrollTop());
          
          if (globalBottomOffset >= 0) {
            scope.$broadcast(prefix + 'scrolledToFooter', globalBottomOffset, element);
          }
        };
        
        $document.scroll(computeOffset);
      },
    };
  }
  
} ());
