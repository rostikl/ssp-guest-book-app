(function () {
  'use strict';
  
  /**
   * appCommentForm directive for create/edit Comment model.
   */
  
  angular.module('app.comment-form')
    .directive('appCommentForm', directive);
  
  function directive () {
    return {
      restrict: 'A',
      replace: true,
      templateUrl: 'app/comments/comment-form/comment-form.html',
    };
  }
  
} ());
