(function (window) {
  'use strict';
  
  /**
   * BACKEND_HOST - URL for connect to app backend server.
   */
  
  var location = window.location;
  
  angular.module('app')
    .constant('BACKEND_HOST', '//' + location.hostname + ':3434');

/* globals window */
} (window));
