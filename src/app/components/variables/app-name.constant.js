(function () {
  'use strict';
  
  angular.module('app')
    .constant('APP_NAME', 'SSP Engineering Guest Book');
    
} ());
